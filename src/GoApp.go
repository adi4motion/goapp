package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
	"os"
	"bufio"
	"strings"
)


type FORECAST_DATA struct {
	Currently struct {
		Temperature float32
		PrecipProbability float32
		PrecipIntensity float32
	}
}

type LOCATION_DATA struct {
	Results []struct {
		Geometry struct {
			Location struct {
				Lat float32
				Lng float32
			}
		}
	}
}

type SUGGESTION_DATA struct {
	Results []struct {
		Name string
		Vicinity string
	}
}

func convertFtoC(value float32) float32 {
	return (value - 32) * 5 / 9
}

func getRequest(url string) []byte {
	start := time.Now()
	response, _ := http.Get(url)
	duration := time.Since(start)
	fmt.Printf("-->Response Time: %.2f seconds\n", duration.Seconds())
	defer response.Body.Close()
	body, _ := ioutil.ReadAll(response.Body)
	return body
}

func checkForWheatherConditions(LAT float32, LNG float32, channel chan FORECAST_DATA) {
	fmt.Printf("\n-->Checking wheather conditions...\n")
	data := getRequest(fmt.Sprintf("https://api.forecast.io/forecast/ce0a7de8c3cd88058ef7e5d58cae6b9a/%f,%f", LAT, LNG))
	var obj FORECAST_DATA
	json.Unmarshal(data, &obj)
	obj.Currently.Temperature = convertFtoC(obj.Currently.Temperature)
	channel <- obj
}

func findGeolocation(location string, channel chan LOCATION_DATA) {
	fmt.Printf("\n-->Finding Geolocation...\n")
	data := getRequest("https://maps.googleapis.com/maps/api/geocode/json?address=" + url.QueryEscape(location) + "&sensor=false")
	var obj LOCATION_DATA
	json.Unmarshal(data, &obj)
	channel <- obj
}

func findPlacesForGeolocation(LAT float32, LNG float32, keyword string, channel chan SUGGESTION_DATA) {
	fmt.Printf("\n-->Searching for places...\n")
	data := getRequest(fmt.Sprintf("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=15000&keyword=" +keyword+ "&sensor=false&key=AIzaSyCyWged1XnV9ffxXYQHsAT_g5w63KmF14s", LAT, LNG))
	var obj SUGGESTION_DATA
	json.Unmarshal(data, &obj)
	channel <- obj
}


func lookForRestaurants(command string) int{
	keywords := []string{"eat", "hungry", "food", "drink", "thirsty"}
	for _,element := range keywords {
		if strings.Index(command, element) != -1 {
			return 1
		}
	}
	return 0
}

func lookForGames(command string) int{
	keywords := []string{"play", "game", "fun", "challenge", "bored", "arcade"}
	for _,element := range keywords {
		if strings.Index(command, element) != -1 {
			return 1
		}
	}
	return 0
}

func lookForMovies(command string) int{
	keywords := []string{"movie", "cinema", "watch", "film", "actor", "imax", "3d"}
	for _,element := range keywords {
		if strings.Index(command, element) != -1 {
			return 1
		}
	}
	return 0
}

func lookForParks(command string) int{
	keywords := []string{"park", "walk", "trees", "nature", "out", "relax"}
	for _,element := range keywords {
		if strings.Index(command, element) != -1 {
			return 1
		}
	}
	return 0
}

const delimiter = '\n'

func main() {
	//start with a default keyword
	keyword := "park";

	//get user location
	fmt.Println("Where are you ?")

	reader := bufio.NewReader(os.Stdin)
	location, err := reader.ReadString(delimiter)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//ask user what de'd like to do
	fmt.Println("\nWhat do you want to do ?")

	schedule, err := reader.ReadString(delimiter)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//check through available options
	if lookForRestaurants(schedule) == 1 {
		keyword = "food"
	}

	if lookForGames(schedule) == 1 {
		keyword = "games"
	}

	if lookForMovies(schedule) == 1 {
		keyword = "cinema"
	}

	if lookForParks(schedule) == 1 {
		keyword = "park"
	}

	//create a channel for getting geolocation
	geoLocation := make(chan LOCATION_DATA)
	go findGeolocation(location, geoLocation)

	//get geolocation data
	var geoLocationData LOCATION_DATA
	geoLocationData = <- geoLocation

	lat := geoLocationData.Results[0].Geometry.Location.Lat
	lng := geoLocationData.Results[0].Geometry.Location.Lng

	//create a changgel for getting forecast data
	forecast := make(chan FORECAST_DATA)
	go checkForWheatherConditions(lat, lng, forecast)

	//get forecast data
	var forecastData FORECAST_DATA
	forecastData = <- forecast

	//show shome messages based on user choice and forecast data
	if keyword == "park" {
		if forecastData.Currently.PrecipProbability >= 0.2 && forecastData.Currently.PrecipProbability <= 0.4 {
			fmt.Printf("\nThere are some slim chanches to rain today. You may want to take an umbrella just in case\n")
		} else if forecastData.Currently.PrecipProbability >= 0 && forecastData.Currently.PrecipProbability < 0.2 && forecastData.Currently.Temperature > 20 {
			fmt.Printf("\nLooks like the sky is clear. Nice wheather for a walk in the park\n")
		} else if forecastData.Currently.PrecipProbability >= 0 && forecastData.Currently.PrecipProbability < 0.2 && forecastData.Currently.Temperature < 20 {
			fmt.Printf("\nLooks like the sky is clear, but temperature is a bit low (%.2f C), make sure you get your coat\n", forecastData.Currently.Temperature)
		} else if forecastData.Currently.PrecipIntensity < 0.05{
			fmt.Printf("\nToday is going to rain. Maybe you should go out to see a movie instead? \n")
			keyword = "cinema"
		} else {
			fmt.Printf("\nSome havy rain is expected. Maybe you should go out to eat someting instead? \n")
			keyword = "food"
		}
	} else {
		if forecastData.Currently.PrecipProbability >= 0 && forecastData.Currently.PrecipProbability <= 0.2 && forecastData.Currently.Temperature > 20 {
			fmt.Printf("\nSome superb wheather is expected today, with temperature of %.2f C. Maybe you'll want to go to the park? \n", forecastData.Currently.Temperature)
			keyword = "park"
		} else if forecastData.Currently.PrecipProbability >= 0 && forecastData.Currently.PrecipProbability <= 0.2 && forecastData.Currently.Temperature < 20{
			fmt.Printf("\nToday's going to be a bit cold outside (%.2f C). Don't forget your coat\n", forecastData.Currently.Temperature)
		} else if forecastData.Currently.PrecipProbability <= 0.5 {
			fmt.Printf("\nThere are some chances of rain today, don't forget your umbrella \n")
		} else if forecastData.Currently.PrecipProbability > 0.5 {
			fmt.Printf("\nSome havy rain is expected, please make sure you have your umbrella with you and some boots \n")
		}
	}

	//create a channel for suggestions
	suggestions := make(chan SUGGESTION_DATA)
	go findPlacesForGeolocation(lat, lng ,keyword, suggestions)

	//get suggestions
	var suggestionsData SUGGESTION_DATA
	suggestionsData = <-suggestions

	//show (max 6) suggestions to user
	fmt.Printf("\nHere are some places you could go:\n\n")

	for index, element := range suggestionsData.Results {
		fmt.Printf(fmt.Sprintf("%s \n", element.Name))
		fmt.Printf(fmt.Sprintf("Address: %s \n\n", element.Vicinity))
		if index > 5{
			break
		}

	}
}
